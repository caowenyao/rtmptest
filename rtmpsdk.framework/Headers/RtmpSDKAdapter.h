#import <Foundation/Foundation.h>

@interface RtmpSDKAdapter : NSObject
-(void)startPlay: (NSString*)uid withUrl:(NSString*)url andWidget:(NSString*)widget;
-(void)stopPlay: (NSString*)url;
@end
